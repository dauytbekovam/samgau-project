CREATE TABLE users
(
    "id"         SERIAL        NOT NULL,
    "login"      VARCHAR(50)   NOT NULL,
    "email"      VARCHAR(50)   NOT NULL,
    "password"   VARCHAR(250)  NOT NULL,
    "role"       VARCHAR(50)   NOT NULL,
    "active"     BOOLEAN       NOT NULL   DEFAULT TRUE,

    CONSTRAINT users_pkey
        PRIMARY KEY(id),

    CONSTRAINT users_email_unique
        UNIQUE (email)
);

insert into "users" (login, email, password, role) VALUES
-- user    - admin@test.com   |  пароль Admin123
-- student - student@test.com |  пароль Student123
    ('admin', 'admin@test.com', '$2a$10$mORwtK4znSqLoEgIlxYFpOFQrSkkgbUO20tUeiyiEiVOFWkVsMgYS', 'ADMIN'),
    ('student', 'student@test.com', '$2a$12$pMwmh09DX205tA3FNXS4ku6mSA6eZloZQj.0bP1B1kjZ9exIwrZU6', 'STUDENT');