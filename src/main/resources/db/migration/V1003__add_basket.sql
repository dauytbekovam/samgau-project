CREATE TABLE "baskets"
(
    "id"           SERIAL          NOT NULL,
    "created"      TIMESTAMP       NOT NULL,
    "updated"      TIMESTAMP       DEFAULT NULL,
    "user_id"      SERIAL          NOT NULL,

    CONSTRAINT baskets_pkey
      PRIMARY KEY(id),

    CONSTRAINT fk_baskets_users
      FOREIGN KEY (user_id)
          REFERENCES users (id)
          ON DELETE RESTRICT
          ON UPDATE CASCADE
);

insert into "baskets" (created, updated, user_id) VALUES
('2022-06-13 04:45:43.728076', null, 2);


CREATE TABLE "baskets_books"
(
    "id"           SERIAL          NOT NULL,
    "created"      TIMESTAMP       NOT NULL,
    "updated"      TIMESTAMP       DEFAULT NULL,
    "basket_id"    SERIAL          NOT NULL,
    "book_id"      SERIAL          NOT NULL,
    "quantity"     INT             NOT NULL ,

    CONSTRAINT baskets_books_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_baskets_baskets_books
        FOREIGN KEY (basket_id)
            REFERENCES baskets (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE,

    CONSTRAINT fk_baskets_books_books
        FOREIGN KEY (book_id)
            REFERENCES books (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);