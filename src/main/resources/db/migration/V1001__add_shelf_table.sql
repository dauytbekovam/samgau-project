CREATE TABLE shelves
(
    "id"           SERIAL          NOT NULL,
    "created"      TIMESTAMP       NOT NULL,
    "updated"      TIMESTAMP       DEFAULT NULL,
    "shelf"        INT             NOT NULL,
    "row"          INT             NOT NULL,

    UNIQUE(shelf, row),

    CONSTRAINT shelves_pkey
        PRIMARY KEY(id)
);

insert into "shelves" (created, updated, shelf, row) VALUES
        ('2022-06-13 04:45:43.728076', null, 1, 1),
        ('2022-06-13 04:45:43.728076', null, 1, 2),
        ('2022-06-13 04:45:43.728076', null, 1, 3),
        ('2022-06-13 04:45:43.728076', null, 2, 1),
        ('2022-06-13 04:45:43.728076', null, 2, 2),
        ('2022-06-13 04:45:43.728076', null, 2, 3);

