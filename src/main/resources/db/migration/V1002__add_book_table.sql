CREATE TABLE books
(
    "id"           SERIAL          NOT NULL,
    "created"      TIMESTAMP       NOT NULL,
    "updated"      TIMESTAMP       DEFAULT NULL,
    "name"         VARCHAR(50)     NOT NULL,
    "author"       VARCHAR(50)     NOT NULL,
    "price"        DECIMAL         NOT NULL,
    "date"         DATE            NOT NULL,
    "library_id"   INT           NOT NULL,

    CONSTRAINT books_pkey
        PRIMARY KEY(id),

    CONSTRAINT fk_books_shelves
        FOREIGN KEY (library_id)
            REFERENCES shelves (id)
            ON DELETE CASCADE
            ON UPDATE CASCADE
);

insert into "books" (created, updated, name, author, price, date, library_id) VALUES
('2022-06-13 04:45:43.728076', null, 'Гарри Поттер', 'Джоан Роулинг', 1000, '2022-06-13', 1),
('2022-06-13 04:45:43.728076', null, 'Зелёная миля', 'Стивен Кинг', 1000, '2022-06-13', 1),
('2022-06-13 04:45:43.728076', null, 'Унесенные ветром', 'Маргарет Митчелл', 1000, '2022-06-13', 1),
('2022-06-13 04:45:43.728076', null, '451° по Фаренгейту', 'Рэй Брэдбери', 1000, '2022-06-13', 1),
('2022-06-13 04:45:43.728076', null, '1984', 'Джордж Оруэлл', 1000, '2022-06-13', 2),
('2022-06-13 04:45:43.728076', null, 'Мастер и Маргарита', 'Михаил Булгаков', 1000, '2022-06-13', 2),
('2022-06-13 04:45:43.728076', null, 'Шантарам', 'Грегори Дэвид Робертс', 1000, '2022-06-13', 2),
('2022-06-13 04:45:43.728076', null, 'Три товарища', 'Эрих Мария Ремарк', 1000, '2022-06-13', 2),
('2022-06-13 04:45:43.728076', null, 'Цветы для Элджернона', 'Дэниел Киз', 1000, '2022-06-13', 3),
('2022-06-13 04:45:43.728076', null, 'Портрет Дориана Грея', 'Оскар Уайльд', 1000, '2022-06-13', 3),
('2022-06-13 04:45:43.728076', null, 'Маленький принц', 'Антуан де Сент-Экзюпери', 1000, '2022-06-13', 3),
('2022-06-13 04:45:43.728076', null, 'Над пропастью во ржи', 'Джером Д. Сэлинджер', 1000, '2022-06-13', 3);
