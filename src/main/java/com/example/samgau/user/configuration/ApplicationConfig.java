package com.example.samgau.user.configuration;

import com.example.samgau.user.services.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Bean
    CommandLineRunner init(StorageService service) {
        return (args) -> {
            service.init();
        };
    }
}
