package com.example.samgau.user.enumiration;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

@Getter
@AllArgsConstructor
public enum UserRole implements GrantedAuthority {

    ADMIN("Админ"), STUDENT("Пользователь");

    private String russianName;

    @Override
    public String getAuthority() {
        return name();
    }
}
