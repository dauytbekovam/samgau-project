package com.example.samgau.user.DTO;

import com.example.samgau.user.models.User;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDTO {
    private Integer id;
    private String login;
    private String email;
    private String password;
    private String role;
    private boolean active;
}
