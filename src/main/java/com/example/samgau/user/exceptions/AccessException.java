package com.example.samgau.user.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@Setter
@ResponseStatus(code = HttpStatus.FORBIDDEN)
public class AccessException extends Exception{
    private String exception = "Доступ закрыт";
}
