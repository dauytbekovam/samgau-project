package com.example.samgau.user.services;

import com.example.samgau.basket.models.Basket;
import com.example.samgau.basket.repositories.BasketRepository;
import com.example.samgau.user.DTO.UserAddDTO;
import com.example.samgau.user.models.User;
import com.example.samgau.user.exceptions.ResourceNotFoundException;
import com.example.samgau.user.exceptions.UserAlreadyExistsException;
import com.example.samgau.user.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.validation.Valid;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository repository;
    private final BasketRepository basketRepository;
    private final PasswordEncoder encoder;

    public User register(UserAddDTO dto){
        if(repository.existsByEmail(dto.getEmail()) || repository.existsByLogin(dto.getLogin()))
            throw new UserAlreadyExistsException();
        User user = User.builder()
                .login(dto.getLogin())
                .email(dto.getEmail())
                .password(encoder.encode(dto.getPassword()))
                .build();
        repository.save(user);
        basketRepository.save(Basket.builder()
                .user(user)
                .build());
        return user;
    }

    public User findByEmail(String email){
        return repository.findByEmail(email).orElseThrow(() -> {
            return new ResourceNotFoundException("Пользователь", email);
        });
    }

    public boolean existByEmail(String email){
        return repository.existsByEmail(email);
    }

    public boolean existByLogin(String login){
        return repository.existsByLogin(login);
    }

    public String registerMapping(@Valid UserAddDTO dto,
                           BindingResult result,
                           RedirectAttributes attributes){
        attributes.addFlashAttribute("dto", dto);
        if (result.hasFieldErrors()){
            attributes.addFlashAttribute("errors", result.getFieldErrors());
            return "redirect:/register";
        }
        else if (existByEmail(dto.getEmail())){
            attributes.addFlashAttribute("emailError", "Пользователь с мейлом "
                    + dto.getEmail() + " существует. Выберите другой");
            return "redirect:/register";
        }
        else if (existByLogin(dto.getLogin())){
            attributes.addFlashAttribute("loginError", "Пользователь с логином "
                    + dto.getLogin() + " существует. Выберите другой");
            return "redirect:/register";
        }
        register(dto);
        attributes.addFlashAttribute("success", "Регистрация прошла успешно! Вам необходимо авторизоваться");
        return "redirect:/login";
    }
}
