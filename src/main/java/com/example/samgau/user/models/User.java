package com.example.samgau.user.models;

import com.example.samgau.user.enumiration.UserRole;
import lombok.Builder;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    @NotNull
    private String login;

    @Column
    @NotNull
    private String email;

    @Column
    @NotNull
    private String password;

    @Column
    @NotNull
    @Builder.Default
    private String role = UserRole.STUDENT.name();

    @Column
    @NotNull
    @Builder.Default
    private boolean active = true;
}
