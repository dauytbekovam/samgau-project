package com.example.samgau.user.controllers;

import com.example.samgau.user.DTO.UserAddDTO;
import com.example.samgau.user.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequiredArgsConstructor
public class UserController {
    private final UserService service;

    @GetMapping("/register")
    public String register(){
        return "user/register";
    }

    @PostMapping("/register")
    public String register(@Valid UserAddDTO dto,
                           BindingResult result,
                           RedirectAttributes attributes){
        return service.registerMapping(dto, result, attributes);
    }

    @GetMapping("/login")
    public String login(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model){
        model.addAttribute("error", error);
        return "user/login";
    }

    @GetMapping("/cabinet")
    public String cabinet(Model model, Principal principal){
        model.addAttribute("user", service.findByEmail(principal.getName()));
        return "user/cabinet";
    }

    @PostMapping("/logout")
    public String logOut(){
        return "redirect:/login";
    }
}
