package com.example.samgau.basket.repositories;

import com.example.samgau.basket.models.BooksBasket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BooksBasketRepository extends JpaRepository<BooksBasket, Integer> {
    List<BooksBasket> getAllByBasketId(Integer id);
    List<BooksBasket> getAllByBookId(Integer id);
    List<BooksBasket> getAllByBasketIdAndBookId(Integer basketId, Integer bookId);
}
