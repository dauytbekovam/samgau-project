package com.example.samgau.basket.services;

import com.example.samgau.basket.DTO.BasketDTO;
import com.example.samgau.basket.DTO.BookInBasketDTO;
import com.example.samgau.basket.DTO.BooksBasketDTO;
import com.example.samgau.basket.models.Basket;
import com.example.samgau.basket.models.BooksBasket;
import com.example.samgau.basket.repositories.BasketRepository;
import com.example.samgau.basket.repositories.BooksBasketRepository;
import com.example.samgau.book.models.Book;
import com.example.samgau.book.repositories.BookRepository;
import com.example.samgau.book.services.BookService;
import com.example.samgau.user.exceptions.ResourceNotFoundException;
import com.example.samgau.user.models.User;
import com.example.samgau.user.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class BasketService {

    private final BasketRepository repository;
    private final UserService userService;
    private final BooksBasketRepository itemsBasketRepository;
    private final BookRepository bookRepository;
    private final BookService bookService;

    public Basket getByUserId(Integer id){
        return repository.findByUserId(id).orElseThrow(() -> {
            return new ResourceNotFoundException("Корзина", id);
        });
    }

    public Basket findByPrincipal(Principal principal){
        User user = userService.findByEmail(principal.getName());
        return repository.findByUserId(user.getId()).orElseThrow(()-> {
            return new ResourceNotFoundException("Корзина", user.getId());
        });
    }

    public BasketDTO addItems(BooksBasketDTO dto, Principal principal){
        Basket basket = findByPrincipal(principal);
        itemsBasketRepository.save(BooksBasket.builder()
                .basket(basket)
                .book(dto.getBook())
                .quantity(dto.getItemQuantity())
                .build());
        return BasketDTO.from(repository.save(basket));
    }

    public Book findOneItem(Integer itemId){
        return bookRepository.findById(itemId).orElseThrow(() -> {
            return new ResourceNotFoundException("Книга", itemId);
        });
    }

    public void deleteItem(Integer itemId){
//        Basket basket = findByPrincipal(principal);
        Book book = findOneItem(itemId);
        List<BooksBasket> all = itemsBasketRepository.getAllByBookId(book.getId());
        for (int i = 0; i < all.size(); i++) {
            if (all.get(i).getBook().getId().equals(book.getId())){
                itemsBasketRepository.delete(all.get(i));
            }
        }
        // удаляет все связи с книгой то есть BooksBasket где есть книга с айди 9
    }

    public Map<BookInBasketDTO, Integer> getAllItems(Integer id){
        List<BooksBasket> all = itemsBasketRepository.getAllByBasketId(id);
        Map<BookInBasketDTO, Integer> items = new HashMap<>();
        for (int i = 0; i < all.size(); i++) {
            items.put(BookInBasketDTO.from(all.get(i).getBook()), getTotalQuantity(id, all.get(i).getBook().getId()));
        }
        return items;
    }

    public Double getTotalPrice(Integer id){
        List<BooksBasket> all = itemsBasketRepository.getAllByBasketId(id);
        double totalPrice = 0;
        for (int i = 0; i < all.size(); i++) {
            Book book = bookService.read(all.get(i).getBook().getId());
            totalPrice += (book.getPrice() * all.get(i).getQuantity()) ;
        }
        return totalPrice;
    }

    public int getTotalQuantity(Integer basketId, Integer bookId){
        List<BooksBasket> all = itemsBasketRepository.getAllByBasketIdAndBookId(basketId, bookId);
        int totalQuantity = 0;
        for (int i = 0; i < all.size(); i++) {
            totalQuantity += all.get(i).getQuantity() ;
        }
        return totalQuantity;
    }

    public void getOne(Model model, Principal principal, HttpSession session){
        User user = userService.findByEmail(principal.getName());
        model.addAttribute("user", user);
        Basket basket = getByUserId(user.getId());
        model.addAttribute("items", getAllItems(basket.getId()));
        model.addAttribute("totalPrice", getTotalPrice(basket.getId()));
        session.setAttribute("items", getAllItems(basket.getId()));
    }
}
