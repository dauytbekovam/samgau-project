package com.example.samgau.basket.models;

import com.example.samgau.book.models.Book;
import com.example.samgau.interfaces.AbstractEntity;
import lombok.*;
import javax.persistence.*;

@Table(name = "baskets_books")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class BooksBasket extends AbstractEntity {

    @ToString.Exclude
    @ManyToOne
    private Basket basket;

    @ToString.Exclude
    @OneToOne
    private Book book;

    private int quantity;
}
