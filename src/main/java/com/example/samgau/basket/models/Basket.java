package com.example.samgau.basket.models;

import com.example.samgau.book.models.Book;
import com.example.samgau.interfaces.AbstractEntity;
import com.example.samgau.user.models.User;
import lombok.*;
import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "baskets")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Basket extends AbstractEntity {

    @ToString.Exclude
    @OneToOne
    private User user;

    @ToString.Exclude
    @OneToMany
    List<Book> books;
}
