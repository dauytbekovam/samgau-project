package com.example.samgau.basket.controllers;

import com.example.samgau.basket.DTO.BooksBasketDTO;
import com.example.samgau.basket.services.BasketService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpSession;
import java.security.Principal;

@Controller
@AllArgsConstructor
@RequestMapping
public class BasketController {

    private final BasketService service;

    @GetMapping("/basket")
    public String getOne(Model model, Principal principal, HttpSession session){
        service.getOne(model, principal, session);
        return "basket/basket";
    }

    @PostMapping("/api/books/{bookId}/addToCart")
    public String addItem(BooksBasketDTO dto, Principal principal){
        service.addItems(dto, principal);
        return "redirect:/basket";
    }
}