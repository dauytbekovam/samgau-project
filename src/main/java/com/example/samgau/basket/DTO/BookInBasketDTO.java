package com.example.samgau.basket.DTO;

import com.example.samgau.book.models.Book;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class BookInBasketDTO {

    private int id;
    private String name;
    private String author;
    private double price;

    public static BookInBasketDTO from(Book book){
        return BookInBasketDTO.builder()
                .id(book.getId())
                .name(book.getName())
                .author(book.getAuthor())
                .price(book.getPrice())
                .build();
    }
}
