package com.example.samgau.basket.DTO;

import com.example.samgau.basket.models.Basket;
import com.example.samgau.book.models.Book;
import com.example.samgau.interfaces.AbstractDTO;
import lombok.Builder;
import lombok.Data;
import javax.persistence.*;

@Data
@Builder
public class BooksBasketDTO extends AbstractDTO {

    @ManyToOne
    private Basket basket;

    @ManyToMany
    private Book book;

    private int itemQuantity;
}
