package com.example.samgau.basket.DTO;

import com.example.samgau.basket.models.Basket;
import com.example.samgau.book.models.Book;
import com.example.samgau.interfaces.AbstractDTO;
import com.example.samgau.user.models.User;
import lombok.Builder;
import lombok.Data;
import java.util.List;

@Builder
@Data
public class BasketDTO extends AbstractDTO {

    private User user;
    private List<Book> books;

    public static BasketDTO from(Basket basket){
        return BasketDTO.builder()
                .user(basket.getUser())
                .books(basket.getBooks())
                .build();
    }
}
