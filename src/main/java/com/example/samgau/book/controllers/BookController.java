package com.example.samgau.book.controllers;

import com.example.samgau.book.DTO.BookAddDTO;
import com.example.samgau.book.models.Book;
import com.example.samgau.book.services.BookService;
import com.example.samgau.book.services.PropertiesService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
@RequestMapping("/api/books")
public class BookController {

    private final BookService service;
    private final PropertiesService propertiesService;

    @GetMapping
    public String all(Model model, Pageable pageable, HttpServletRequest uriBuilder){
        service.getAll(model, pageable, uriBuilder);
        return "book/books";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/add/{shelfId}")
    public String create(@PathVariable int shelfId,
                         @Valid BookAddDTO dto, BindingResult validationResult, RedirectAttributes attributes){
        return service.postCreate(shelfId, dto, validationResult, attributes);
    }

    @GetMapping("/{id}")
    public String read(@PathVariable Integer id, Model model){
        model.addAttribute("book", service.read(id));
        return "book/oneBook";
    }

    @RequestMapping(path = "/api/books", params = "search", method = RequestMethod.GET)
    public String readAll(@RequestParam String search, Pageable pageable, HttpServletRequest uriBuilder,
                      Model model){
        Page<Book> books = service.filter(pageable, search);
        model.addAttribute("books", books);
        String uri = uriBuilder.getRequestURI();
        PageableView.constructPageable(books, propertiesService.getDefaultPageSize(), model, uri);
        return "book/books";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/update/{id}")
    public String update(@PathVariable int id,
                         @Valid BookAddDTO dto, BindingResult validationResult, RedirectAttributes attributes){
        return service.postUpdate(id, dto, validationResult, attributes);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/{id}/delete")
    public String delete(@PathVariable Integer id){
        service.delete(id);
        return "redirect:/api/shelves";
    }
}
