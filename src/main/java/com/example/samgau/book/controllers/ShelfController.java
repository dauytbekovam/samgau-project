package com.example.samgau.book.controllers;

import com.example.samgau.book.DTO.ShelfAddDTO;
import com.example.samgau.book.services.BookService;
import com.example.samgau.book.services.PropertiesService;
import com.example.samgau.book.services.LibraryService;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

@Controller
@RequiredArgsConstructor
@RequestMapping("/api/shelves")
public class ShelfController {

    private final LibraryService service;
    private final BookService bookService;

    @GetMapping
    public String all(Model model, Pageable pageable, HttpServletRequest uriBuilder){
        service.getAll(model, pageable, uriBuilder);
        return "shelf/shelves";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/postAdd")
    public String create(@Valid ShelfAddDTO dto,
                         BindingResult validationResult,
                         RedirectAttributes attributes){
        return service.createPost(dto, validationResult, attributes);
    }

    @GetMapping("/{id}")
    public String read(@PathVariable Integer id, Model model, Pageable pageable){
        model.addAttribute("books", bookService.filterByShelf(pageable, service.read(id)));
        model.addAttribute("shelf", service.read(id));
        return "shelf/shelf";
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/update/{id}")
    public String update(@PathVariable int id,
                         @Valid ShelfAddDTO dto,
                         BindingResult validationResult,
                         RedirectAttributes attributes){
        return service.updatePost(id, dto, validationResult, attributes);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/{id}/delete")
    public String delete(@PathVariable Integer id){
        service.delete(id);
        return "redirect:/api/shelves";
    }
}
