package com.example.samgau.book.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.hibernate.validator.constraints.Range;
import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
public class ShelfAddDTO {

    @NotNull(message = "Номер полки не может быть null")
    @Range(min=0, max=90, message = "Номер полки не может быть отрицательным числом")
    private Integer shelf;

    @NotNull(message = "Номер ряда не может быть null")
    @Range(min=0, max=90, message = "Номер ряда не может быть отрицательным числом")
    private Integer row;
}
