package com.example.samgau.book.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Getter
@AllArgsConstructor
public class BookAddDTO {

    @NotNull(message = "Номер полки не может быть null")
    @Size(min = 2, message = "Минимальная длина наименования - два символа")
    private String name;

    @NotNull(message = "Номер полки не может быть null")
    @Size(min = 2, message = "Минимальная длина наименования - два символа")
    private String author;

    @NotNull(message = "Номер полки не может быть null")
    @Range(min=0, message = "цена не может быть отрицательным числом")
    private Double price;

    @NotNull(message = "Номер полки не может быть null")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;
}
