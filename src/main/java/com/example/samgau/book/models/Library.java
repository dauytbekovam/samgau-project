package com.example.samgau.book.models;

import com.example.samgau.interfaces.AbstractEntity;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Table(name = "shelves")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Library extends AbstractEntity {

    @Column
    @NotNull
    private Integer shelf;

    @Column
    @NotNull
    private Integer row;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "library")
    @Builder.Default
    @ToString.Exclude
    List<Book> books = new ArrayList<>();
}
