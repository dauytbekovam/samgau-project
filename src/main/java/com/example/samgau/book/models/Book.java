package com.example.samgau.book.models;

import com.example.samgau.interfaces.AbstractEntity;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@Table(name = "books")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Book extends AbstractEntity {

    @Column
    @NotNull
    private String name;

    @Column
    @NotNull
    private String author;

    @Column
    @NotNull
    private Double price;

    @Column
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate date;

    @NotNull
    @ManyToOne
    private Library library;
}
