package com.example.samgau.book.services;

import com.example.samgau.basket.models.BooksBasket;
import com.example.samgau.basket.repositories.BasketRepository;
import com.example.samgau.basket.services.BasketService;
import com.example.samgau.book.DTO.ShelfAddDTO;
import com.example.samgau.book.controllers.PageableView;
import com.example.samgau.book.exceptions.BookNotFoundException;
import com.example.samgau.book.models.Book;
import com.example.samgau.book.models.Library;
import com.example.samgau.book.repositories.BookRepository;
import com.example.samgau.book.repositories.LibraryRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Service
@AllArgsConstructor
public class LibraryService {

    private final LibraryRepository repository;
    private final BookService service;
    private final BasketService basketService;
    private final PropertiesService propertiesService;

    public Library create(ShelfAddDTO dto){
        return repository.save(Library.builder()
                .shelf(dto.getShelf())
                .row(dto.getRow())
                .build());
    }

    public Library read(int id){
        return repository.findById(id).orElseThrow(() -> {
            return new BookNotFoundException("Library with id: ", id);
        });
    }

    public Page<Library> readAll(Pageable pageable){
        return repository.findAll(pageable);
    }

    public Library update(int id, ShelfAddDTO dto){
        Library library = read(id);
        library.setShelf(dto.getShelf());
        library.setRow(dto.getRow());
        repository.save(library);
        return library;
    }

    public void delete(int id){
        Library library = read(id);
        List<Book> books = service.filterByShelf(library);
        for (int i = 0; i < books.size(); i++) {
            basketService.deleteItem(books.get(i).getId());
            service.delete(books.get(i).getId());
        }
        repository.delete(library);
    }

    public boolean existsByShelfAndRow(Integer shelf, Integer raw){
        return repository.existsByShelfAndRow(shelf, raw);
    }

    public void getAll(Model model, Pageable pageable, HttpServletRequest uriBuilder){
        Page<Library> shelves = readAll(pageable);
        model.addAttribute("shelves", shelves);
        String uri = uriBuilder.getRequestURI();
        PageableView.constructPageable(shelves, propertiesService.getDefaultPageSize(), model, uri);
    }

    public String createPost(@Valid ShelfAddDTO dto,
                             BindingResult validationResult,
                             RedirectAttributes attributes){
        if(existsByShelfAndRow(dto.getShelf(), dto.getRow())){
            attributes.addFlashAttribute("existLibraryError", "Такая полка с таким рядом уже существует");
            return "redirect:/api/shelves";
        }
        attributes.addFlashAttribute("dto", dto);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/api/shelves";
        }
        create(dto);
        return "redirect:/api/shelves";
    }

    public String updatePost(@PathVariable int id,
                             @Valid ShelfAddDTO dto,
                             BindingResult validationResult,
                             RedirectAttributes attributes){
        Library library = read(id);
        if(existsByShelfAndRow(dto.getShelf(), dto.getRow())){
            attributes.addFlashAttribute("existLibraryError", "Такая полка с таким рядом уже существует");
            return "redirect:/api/shelves/" + library.getId();
        }
        attributes.addFlashAttribute("dto", dto);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/api/shelves/" + library.getId();
        }
        update(id, dto);
        return "redirect:/api/shelves/" + library.getId();
    }
}
