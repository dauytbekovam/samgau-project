package com.example.samgau.book.services;

import com.example.samgau.book.DTO.BookAddDTO;
import com.example.samgau.book.controllers.PageableView;
import com.example.samgau.book.exceptions.BookNotFoundException;
import com.example.samgau.book.models.Library;
import com.example.samgau.book.repositories.BookRepository;
import com.example.samgau.book.models.Book;
import com.example.samgau.book.repositories.LibraryRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Service
@AllArgsConstructor
public class BookService {
    
    private final BookRepository repository;
    private final LibraryRepository libraryRepository;
    private final PropertiesService propertiesService;

    public Library readLibrary(int id){
        return libraryRepository.findById(id).orElseThrow(() -> {
            return new BookNotFoundException("Library with id: ", id);
        });
    }

    public Book create(BookAddDTO dto, Library library){
        return repository.save(Book.builder()
                .name(dto.getName())
                .author(dto.getAuthor())
                .date(dto.getDate())
                .price(dto.getPrice())
                .library(readLibrary(library.getId()))
                .build());
    }

    public Book read(int id){
        return repository.findById(id).orElseThrow(() -> {
            return new BookNotFoundException("Book with id: ", id);
        });
    }

    public Page<Book> readAll(Pageable pageable){
        return repository.findAll(pageable);
    }

    public Book update(int id, BookAddDTO dto){
        Book book = read(id);
        book.setName(dto.getName());
        book.setAuthor(dto.getAuthor());
        book.setPrice(dto.getPrice());
        book.setDate(dto.getDate());
        repository.save(book);
        return book;
    }

    public void delete(int id){
        Book book = read(id);
        repository.delete(book);
    }

    public Page<Book> filter(Pageable pageable, String name){
        return repository.findAllByNameContains(pageable, name);
    }

    public Page<Book> filterByShelf(Pageable pageable, Library library){
        return repository.findAllByLibrary_ShelfAndLibrary_Row(library.getShelf(), library.getRow(), pageable);
    }

    public List<Book> filterByShelf(Library library){
        return  repository.findAllByLibrary_ShelfAndLibrary_Row(library.getShelf(), library.getRow());
    }

    public void getAll(Model model, Pageable pageable, HttpServletRequest uriBuilder){
        Page<Book> books = readAll(pageable);
        model.addAttribute("books", books);
        String uri = uriBuilder.getRequestURI();
        PageableView.constructPageable(books, propertiesService.getDefaultPageSize(), model, uri);
    }

    public String postCreate(@PathVariable int shelfId,
                             @Valid BookAddDTO dto, BindingResult validationResult, RedirectAttributes attributes){
        attributes.addFlashAttribute("dto", dto);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors",
                    validationResult.getFieldErrors());
            return "redirect:/api/shelves/" + shelfId;
        }
        Library library = readLibrary(shelfId);
        create(dto, library);
        return "redirect:/api/shelves/" + shelfId;
    }

    public String postUpdate(@PathVariable int id,
                             @Valid BookAddDTO dto, BindingResult validationResult, RedirectAttributes attributes){
        Book book = read(id);
        attributes.addFlashAttribute("dto", dto);
        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/api/books/" + book.getId();
        }
        update(id, dto);
        return "redirect:/api/books/" + book.getId();
    }
}
