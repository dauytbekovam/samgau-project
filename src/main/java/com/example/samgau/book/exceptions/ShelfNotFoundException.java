package com.example.samgau.book.exceptions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@Setter
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class ShelfNotFoundException extends RuntimeException {
    private String resource;
    private int id;
    private String email;

    public ShelfNotFoundException(String resource, int id) {
        super();
        this.resource = resource;
        this.id = id;
    }
}
