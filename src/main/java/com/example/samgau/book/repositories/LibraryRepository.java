package com.example.samgau.book.repositories;

import com.example.samgau.book.models.Library;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LibraryRepository extends JpaRepository<Library, Integer> {
    Page<Library> findAllByShelf(Pageable pageable, Integer shelf);
    boolean existsByShelfAndRow(Integer shelf, Integer row);
}
