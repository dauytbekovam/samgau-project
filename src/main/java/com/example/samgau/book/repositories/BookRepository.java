package com.example.samgau.book.repositories;

import com.example.samgau.book.models.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import javax.validation.constraints.NotNull;
import java.util.List;

@Repository
public interface BookRepository  extends JpaRepository<Book, Integer> {
    Page<Book> findAll(Pageable pageable);
    Page<Book> findAllByNameContains(Pageable pageable, String name);
    Page<Book> findAllByLibrary_ShelfAndLibrary_Row(@NotNull Integer library_shelf, @NotNull Integer library_row, Pageable pageable);
    List<Book> findAllByLibrary_ShelfAndLibrary_Row(@NotNull Integer library_shelf, @NotNull Integer library_row);
}
